package com.example.pirvelidavaleba.Fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.example.pirvelidavaleba.R

class FirstFragment : Fragment(R.layout.fragment_first) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val loginButton = view.findViewById<Button>(R.id.button1fragment)
        val saxeliEditText = view.findViewById<EditText>(R.id.editText1fragment)

        val controller = Navigation.findNavController(view)

        loginButton.setOnClickListener {
            val saxeli = saxeliEditText.text.toString()

            if (saxeli.isEmpty()) {
                return@setOnClickListener
            }

            val action = FirstFragmentDirections.actionFragmentFirstToFragmentSecond()
            controller.navigate(action)
        }
    }
}